cd /tmp/asciandr/
setupATLAS
asetup 21.2.82.0,AthDerivation
git clone ssh://git@gitlab.cern.ch:7999/atlas/athena.git
cd athena
git checkout release/21.2.82
mkdir ../build && cd ../build
cp ../athena/Projects/WorkDir/package_filters_example.txt ../package_filters.txt
vim ../package_filters.txt
insert + PhysicsAnalysis/DerivationFramework/DerivationFrameworkMCTruth
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir
make -j8
cd ..
mkdir run
cd run
source ../build/x86_64-slc6-gcc62-opt/setup.sh

Reco_tf.py --inputEVNTFile /eos/atlas/user/a/asciandr/Sherpa228/default_generation_output/output.EVNT.0.root --outputDAODFile CLUSTER4.test.pool.root --reductionConf TRUTH3
