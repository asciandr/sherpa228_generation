#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
source $AtlasSetup/scripts/asetup.sh 21.6.10,AthGeneration,64

export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_88/MCGenerators/openloops/2.0.0/x86_64-slc6-gcc62-opt/lib:$LD_LIBRARY_PATH
export JOBOPTSEARCHPATH=/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common:$JOBOPTSEARCHPATH

PWD=/afs/cern.ch/user/a/asciandr/work/Hbb/new_Sherpa228/
#jobOpt=${PWD}/JOs/MC15.999999.Sherpa_228_NNPDF30NNLO_Zbb_EnhLogPtV.py
jobOpt=${PWD}/JOs/JOs/MC15.999999.Sherpa_228_NNPDF30NNLO_Zbb_EnhLogPtV_HtPrime.py
job=0
gridPack=${PWD}/integrations/group.phys-gener.sherpa020208.999999.Sherpa_228_NNPDF30NNLO_Zbb_EnhLogPtV_13TeV.TXT.mc15_v1._00001.tar.gz
output_dir=${PWD}/generation_output/

echo "Going to generate Z->bb events..."
#Generate_tf.py --ecmEnergy=13000 --firstEvent=1 --randomSeed=14567 --runNumber=999999 --jobConfig=$jobOpt --outputEVNTFile=output.EVNT.${job}.root --inputGenConfFile=$gridPack
Generate_tf.py --ecmEnergy=13000 --firstEvent=1 --randomSeed=14567 --runNumber=999999 --jobConfig=$jobOpt --outputEVNTFile=output.EVNT.${job}.root --rivetAnas=MC_XS,MC_JETS --inputGenConfFile=$gridPack

echo "Going to copy products to output folder..."
ls -haltr
cp log.generate $output_dir
cp output.EVNT.${Job}.root *.yoda $output_dir
