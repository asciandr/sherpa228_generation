#!/bin/bash

events_per_job=200

for i in {0..500}
    do
        echo "JOb n. $i"
        echo "Random seed: $RANDOM"
        cat commands_htcondor.sh | sed "s/XNJOB/$i/g"  | sed "s/XRANDOM_SEED/$RANDOM/g"  | sed "s/XMAXEVENTS/$events_per_job/g" > commands_htcondor_${i}.sh
        chmod 755 commands_htcondor_${i}.sh
        cat hello_htcondor.sub | sed "s/XJOBSH/commands_htcondor_${i}.sh/g" > hello_htcondor_${i}.sub
        condor_submit hello_htcondor_${i}.sub
    done

#condor_submit hello.sub 
