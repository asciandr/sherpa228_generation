
include("Sherpa_i/2.2.8_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa Z -> bb + 2j@NLO + 4j@LO with 1000 GeV < pTV < ECMS GeV."
evgenConfig.keywords = ["SM", "Z", "jets" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch", "jpasner@ucsc.edu" ]
evgenConfig.minevents = 200

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])+MPerp(p[2]))/4};

  CSS_REWEIGHT 1
  REWEIGHT_SPLITTING_ALPHAS_SCALES 1
  REWEIGHT_SPLITTING_PDF_SCALES 1
  CSS_REWEIGHT_SCALE_CUTOFF 5

  %tags for process setup
  NJET:=3; LJET:=1,2,3; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  HDH_STATUS[23,5,-5]=2

  METS_BBAR_MODE=3
  NLO_CSS_PSMODE=1

}(run)

(processes){
  Process 93 93 -> 23 93 93{NJET};
  Enhance_Observable VAR{log10(PPerp(p[3]))}|2.4|3 {3,4,5,6,7}
  Cut_Core 1;
  Order (*,1); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {5};
  Max_Epsilon 0.01 {5};
  Integration_Error 0.05 {2,3,4,5};
  End process;
}(processes)

(selector){
  PTNLO 23 250.0 E_CMS 
}(selector)

"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[23]=0" ]
genSeq.Sherpa_i.Parameters += [
  "FRAGMENTATION=Lund",
  "DECAYMODEL=Lund",
  "PARJ(21)=0.36",
  "PARJ(41)=0.3",
  "PARJ(42)=0.6"
]
genSeq.Sherpa_i.NCores = 16
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppz", "ppzj", "ppzjj" ]
