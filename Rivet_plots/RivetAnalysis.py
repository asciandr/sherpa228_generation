theApp.EvtMax = -1

import os
import AthenaPoolCnvSvc.ReadAthenaPool

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'MC_XS' ]
rivet.Analyses += [ 'MC_JETS' ]
rivet.RunName = ""
rivet.HistoFile = "myanalysis"
job += rivet
